# Evaluación de Rendimiento
------------------------------------------------------------------------------------------
								|  Exp  | 04-05-2018 |  Exp  |
	Back Squat: 2 Max Rep (PR)  |  225  |   296 lb   |  335  |
    DeadLift:   1 Max Rep (PR)  |  285  |   336 lb   |  375  |
    Pull Ups:   AMRAP Unbroken	|  02   |   02       |  03   |
    Burpees:    90 s AMRAP      |  15   |   28       |  32   |
    Assault:    10 min AMRAP    |  100  |   100 cal  |  120  |
------------------------------------------------------------------------------------------
