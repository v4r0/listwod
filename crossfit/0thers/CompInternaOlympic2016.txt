/****************************************************************************/
Wod 1
- For Time (TC: 12 min)
    · 350 mt Row
    21 - 15 - 09
        · DeadLift (135 lb)
        · Box Jump Over (60 cm)
    · 350 mt Row
/****************************************************************************/
Wod 2.a
- AMRAP 5 min
    2 - 4 - 6... (+2)
    · Power Clean (75 lb)
    · Thruster (75 lb)
    · Burpees over Bar
Wod 2.b
- Rep Max
    · 01 Power Clean
/****************************************************************************/
Wod 3
- For Time
    · 25 GHD Sit Ups
    · 25 Wall Ball (5 kg)
    · 25 AirDyne Cal
/****************************************************************************/
Wod 4
- For Time (8 min)
    · 15 Knees to Elbow
    · 15 Lunge with Bumper OverHead (25 lb)
    · 400 mt Rut
    · 15 Knees to Elbow
    · 15 Lunge with Bumper OverHead (25 lb)
/****************************************************************************/
