/****************************************************************************/
Lunes 01-04-2019
- 3 Sets
    · 03 Back Squat T:3-1-3 (185 lb)
    · 06 Heavy Dumbbell Snatch (30 kg)
    · 20 s Hang L Sit Hold
- 5 Sets (3 min per set)
    · 30 s Assault ~80%
    · 06 Dumbbell Over Head Walking Lunge (15 kg)
    · 15 Toes to Bar / 20 Knees to Elbow
    · 06 Dumbbell Over Head Walking Lunge (15 kg)
    · 10 Burpees
- 3 Sets
    · 15 One Leg Glute Bridge
/****************************************************************************/
Martes 02-04-2019
- Kipping Technique
    · Hollow
    · Hang Hollow
    · SuperMan to Hollow
- EMOM 12 min
    · 04 Power Clean + 04 Hang Power Clean (115 lb)
    · 12 (6+6) Dumbbell Push Press (20 kg)
    · 10 DBall Squat (100 lb)
- For Time (TC:10 min)
    · 25 DBall Clean (100 lb)
    · 25 Toes to Bar
    · 12 Right Hand Dumbbell Hang Power Clean & Jerk (15 kg)
    · 12 Left Hand Dumbbell Hang Power Clean & Jerk (15 kg)
    · 25 Toes to Bar
    · 25 DBall Clean (100 lb)
- 3 Sets
    · 12 Feet Rise Bear Hold
/****************************************************************************/
Viernes 05-04-2019
- 5 Sets
    · 02 Power Clean + 02 Hang Squat Clean (145 lb)
    · 10 Double Kettlebell Russian Swing (24 kg)
- 5 Sets (3 min per set)
    · 08 Hang Power Clean (95 lb)
    · 20 Jumping Lunge
    · 12 mt Skipping Walk
    · 12 mt DBall Carry (100 lb)
    · 08 DBall Clean & Jerk (80 lb)
/****************************************************************************/
Sábado 06-04-2019
- 3 Sets
    · 3 Over Head Squat T:4-4-1
    > 1st: 045 lb        > 2nd: 065 lb
    > 3rd: 075 lb
- 7 Sets
    · 01 Power Snatch
    > 1st: 065 lb        > 2nd: 075 lb
    > 3rd: 085 lb        > 4th: 095 lb
    > 5th: 105 lb        > 6th: 115 lb
    > 7th: 115 lb
- 7 Sets
    · 01 Power Clean
    > 1st: 115 lb        > 2nd: 135 lb
    > 3rd: 145 lb        > 4th: 165 lb
    > 5th: 175 lb        > 6th: 195 lb
    > 7th: 195 lb
- 2 Sets
    · 02 Squat Clean (165 lb)
- 2 Sets
    · 02 Hang Squat Clean (165 lb)
- 2 Sets
    · 02 Short Hang Squat Clean (95 lb)
- 5 Sets
    · 03 Right Hand Dumbbell Push Press (25 kg)
    · 03 Right Hand Dumbbell Hang Snatch (25 kg)
    · 03 Left Hand Dumbbell Push Press (25 kg)
    · 03 Left Hand Dumbbell Hang Snatch (25 kg)
- 5 Sets
    · 06 (3+3) Dumbbell Snatch (30 kg)
    · 03 DBall Clean (120 lb)
    · 01 DBall Clean & Jerk (80 lb)
/****************************************************************************/
Lunes 08-04-2019
- 3 Sets
    · 03 Back Squat T:3-3-3 (165 lb)
    · 06 (3+3) Dumbbell Push Press (25 kg)
    · 20 s L Hang Hold
- 4 Sets (4 min per set)
    · 60 s Row
    · 12 Double Dumbbell Push Press (15 kg)
    · 12 Double Dumbbell Front Squat (15 kg)
    · 12 Double Dumbbell Thruster (15 kg)
    · 10 Toes to Bar
- 3 Sets
    · 08 Rumanian Double Dumbbell High Row (5 kg)
/****************************************************************************/
Martes 09-04-2019
- to RM
    · 01 Squat Clean (205 lb)
- EMOM 12 min
    · 50 s Row
    · 01 Squat Clean + 02 Front Squat + 8 Burpees over Bar (155 lb)
- AMRAP 10 min
    · 10 Lateral Paralel Jump
    4 - 8 - 12 - 16 - 20 ..+4
    · Dumbbell Snatch (20 kg)
    · Wall Ball (9 kg)
    · Toes to Bar
/****************************************************************************/
Jueves 11-04-2019
- For Time in Pairs
    · 50 Power Clean
    > 01 - 40 (135 lb)      > 40 - 50 (155 lb)
- For Time
    · 20 Toes to Bar
    · 20 Burpees
    · 100 Air Squat
    · 02 Squat Clean (185 lb)
    · 100 Air Squat
    · 20 Burpees
    · 20 Toes to Bar
    · 03 Squat Clean (185 lb)
    · 20 Dumbbell Thrusters (17.5 kg)
    · 04 Squat Clean (185 lb)
    · 100 Air Squat
/****************************************************************************/
Viernes 12-04-2019
- 3 Sets
    · 01 Power Clean + 01 Hang Power Clean + 04 Front Squat (155 lb)
    · 10 Double Dumbbell Hang to Over Head (15 kg)
    · 20 s (10+10) One Leg Kettlebell Swap Hand (20 kg)
- 3 Sets
    · 3 Complex
        > 02 Hang Power Clean + 03 Front Squat (125 lb)
    · 15 Lateral Paralel Jump
    · 14 (7+7) One Hand Dumbbell Hang to Over Head (15 kg)
    · 10 Dumbbell DeadLift (15 kg)
    · 06 DBall Clean & Jerk (70 lb)
/****************************************************************************/
Sábado 13-04-2019
- 6 Sets
    · 01 Over Head Squat T:3-3-3
    > 1st: 065 lb        > 2nd: 075 lb
    > 3rd: 085 lb        > 4th: 095 lb
    > 5th: 105 lb        > 6th: 115 lb
- 7 Sets
    · 01 Split Jerk
    > 1st: 065 lb        > 2nd: 075 lb
    > 3rd: 085 lb        > 4th: 095 lb
    > 5th: 105 lb        > 6th: 115 lb
    > 7th: 135 lb
- 5 Sets
    · 03 Power Clean + 02 Front Squat
    > 1st: 115 lb        > 2nd: 135 lb
    > 3rd: 145 lb        > 4th: 155 lb
    > 5th: 165 lb
- 5 Sets
    · 01 Squat Clean + 01 Push Jerk
    > 1st: 115 lb        > 2nd: 135 lb
    > 3rd: 145 lb        > 4th: 155 lb
    > 5th: 165 lb
- 4 Sets
    · 03 Dumbbell Hang to Over Head (20 kg)
    · 03 DeadLift (215 lb)
    · 01 Double Dumbbell Clean + 01 Push Press (20 kg)
    > 03 DeadLift (275 lb)
- 3 Sets
    · 05 DeadLift (165 lb)
    · 06 Dumbbell Thruster (17.5 kg)
    · 06 Dumbbell Push Press (17.5 kg)
    · 06 Dumbbell Ground to Over Head (17.5 kg)
    · 03 Dumbbell Power Clean + Front Squat (17.5 kg)
    · 03 DBall Clean (120 lb)
    > 1 min rest
/****************************************************************************/
Lunes 15-04-2019
- 4 Sets
    · 03 Front Squat T:3-3-3 (145 lb)
    · 10 Double Kettlebell Russian Swing (24 kg)
- 4 Sets (4 min per set)
    · 45 s Assault ~80%
    · 10 Double Kettlebell Russian Swing (20 kg)
    · 07 Double Kettlebell Front Squat (20 kg)
    · 10 Burpees Over Paralel
    · 14 Wall Ball (9 kg)
- 4 Sets
    · 20 s Hollow
/****************************************************************************/
Martes 16-04-2019
- For Time in Pairs
    · 70 DBall Clean (100 lb)
- EMOM 12 min
    · 06 Clean & Jerk (95 lb)
    · 12 (6+6) Dumbbell Push Press (20 kg)
- 4 Sets
    · 20 Row Pull
    · 16 Thruster (75 lb)
    · 08 Devil Press (15 kg)
    2 - 3 - 4 - 5
    · DBall Clean (120 lb)
    · DBall Squat (120 lb)
/****************************************************************************/
Jueves 18-04-2019
- 3 Sets
    · 08 Dumbbell Clean & Jerk (15 kg)
    · 08 Dumbbell Front Squat (15 kg)
    · 80 Bumper Jump (25 lb)
    · 08 DBall Clean & Jerk (80 lb)
- EMOM 6 min
    · 50 s Dumbbell DeadLift (15 kg)
    · 50 s DBall March (100 lb)
- For Time
    02 - 04 - 06 - 08 - 10 - 12 - 14 - 16
    · Wall Ball (9 kg)
    · Toes to Bar
    · American Swing (24 kg)
/****************************************************************************/
Lunes 22-04-2019
- 3 Sets
    · 03 Back Squat T:2-3-2 (165 lb)
    · 06 Glute Bridge Shoulder Press T:3-1-1 (20 kg)
- 4 Sets (4 min per set)
    · 30 s Assault ~100%
    14 - 16 - 18 - 20
    · Air Squat
    · 08 Burpees to Bar
    14 - 16 - 18 - 20
    · Sit Up
    05 - 06 - 07 - 08
    · DBall Clean (100 lb)
/****************************************************************************/
Martes 23-04-2019
- 3 Sets
    · 06 Ring Push Ups
    · 06 Paralel Dips
    · 12 Box Shoulder Tap
- EMOM 12 min
    · 17 GHD Sit Ups
    · 14 Barbell Push Press (75 lb)
    · 08 DBall Clean (100 lb)
    · 03 Wall Climb
- AMRAP 13 min
    02 - 04 - 06 - (+2)
    · Devil Press (15 kg)
    · DBall Squat (100 lb)
    · Jumping Squat
    · 01 Squat Clean (165 lb)
/****************************************************************************/
Jueves 25-04-2019
- EMOM 6 min
    · 06 Squat Clean (135 lb)
    · 08 Front Squat (135 lb)
- For Time (in pairs)
    · 200 Wall Ball (9 kg)
    · Row while other Work
- 3 Sets
    · 15 Toes to Bar
    · 15 Dumbbell Clean & Jerk LH (20 kg)
    · 100 Air Squat
    01 - 02 - 03
    · Squat Clean (185 lb)
    · 15 Dumbbell Clean & Jerk RH (20 kg)
/****************************************************************************/
Viernes 26-04-2019
- 3 Sets
    · 05 DBall Squat T:3-1-3 (100 lb)
    · 10 Double Dumbbell Hang to Over Head (15 kg)
    · 09 Paralel Jump Stoping
- 5 Sets
    · 12 Cal Assault
    · 10 Double Dumbbell Hang to Over Head (15 kg)
    · 15 Paralel Jump
    · 15 Toes to Bar
    · 03 DBall Clean (120 lb)
/****************************************************************************/
Sábado 27-04-2019
- 7 Sets
    · 01 Over Head Squat
    > 1st: 065 lb        > 2nd: 075 lb
    > 3rd: 085 lb        > 4th: 095 lb
    > 5th: 105 lb        > 6th: 115 lb
    > 7th: 125 lb
- 7 Sets
    · 01 Squat Snatch
    > 1st: 065 lb        > 2nd: 075 lb
    > 3rd: 085 lb        > 4th: 095 lb
    > 5th: 105 lb        > 6th: 115 lb
    > 7th: 125 lb
- 7 Sets
    · 01 Power Clean
    > 1st: 115 lb        > 2nd: 145 lb
    > 3rd: 155 lb        > 4th: 165 lb
    > 5th: 175 lb        > 6th: 185 lb
    > 7th: 185 lb
- 5 Sets
    · 02 DeadLift
    > 1st: 225 lb        > 2nd: 255 lb
    > 3rd: 275 lb        > 4th: 305 lb
    > 5th: 355 lb
- 5 Sets
    · 05 Fat Bar DeadLift (120 lb)
    · 03 DBall Squat (100 lb)
    · 03 Complex
        · 01 Double Dumbbell Push Press + 01 Thrusters (20 kg)
    · 03 DBall Clean & Jerk (100 lb)
/****************************************************************************/
Lunes 29-04-2019
- 3 Sets
    · 06 Back Squat T:1-2-1 (165 lb)
    · 12 (6+6) Knee Arnold Press T:2-2-2 (12.5 kg)
    · 06 (3+3) Reverse Leg to Front 90-90 Position T:5-5 / 10-10
- 5 Sets
    · 12 Dumbbell Push Press (17.5 kg)
    · 07 DBall Clean (100 lb)
    · 12 Dumbbell Thrusters (17.5 kg)
    · 7 DBall Clean (100 lb)
/****************************************************************************/
Martes 30-04-2019
- 4 Sets
    · 01 Complex (UnBroken)
        · 03 DeadLift (115 lb)
        · 03 Hang Power Clean (115 lb)
        · 03 Front Squat (115 lb)
        · 01 Split Jerk (115 lb)
        · 01 Thruster (115 lb)
    · 06 Parallel Push Up T:3-1-3
- For Time (in pairs)
    · 120 Cal Assault
    · 120 Push Up
- 5 Sets
    · 12 Toes to Bar
    · 12 Wall Ball (9 kg)
    · 12 American Swing (20 kg)
- For Time
    · 50 Burpee
- For Time
    · 100 Sit Up
/****************************************************************************/
Jueves 02-05-2019
- EMOM 8 min
    · 06 Power Clean (115 lb)
    · 12 (6+6) Hand Dumbbell Hang Split Snatch (20 kg)
- 3 Sets
    · 30 s Inverted Ring Hold
    · 20 s Clean Reception Hold (145 lb)
    · 20 s Dumbbell Over Head Hold Left Hand (30 kg)
    · 20 s Dumbbell Over Head Hold Right Hand (30 kg)
- 4 Sets
    · 10 DBall Clean (80 lb)
    · 20 Toes to Bar
    · 30 Dumbbell Snatch (17.5 kg)
    · 40 Sit Ups
    · 50 Air Squat
    · 02 Squat Clean (85 kg)
/****************************************************************************/
Sábado 04-05-2019
- 7 Sets
    · 01 Power Snatch
    > 1st: 065 lb        > 2nd: 075 lb
    > 3rd: 085 lb        > 4th: 095 lb
    > 5th: 105 lb        > 6th: 115 lb
    > 7th: 115 lb
- 7 Sets
    · 01 Over Head Squat
    > 1st: 065 lb        > 2nd: 075 lb
    > 3rd: 085 lb        > 4th: 095 lb
    > 5th: 105 lb        > 6th: 115 lb
    > 7th: 115 lb
- 5 Sets
    · 01 Squat Clean + 01 Front Squat
    > 1st: 115 lb        > 2nd: 135 lb
    > 3rd: 155 lb        > 4th: 175 lb
    > 5th: 185 lb
- 3 Sets
    · 02 DeadLift
    > 1st: 225 lb        > 2nd: 315 lb
    > 3rd: 335 lb
- 5 Sets
    · 03 DeadLift Snatch Grip T:1-3-1 (225 lb)
    · 06 Double Dumbbell Ground to Over Head (17.5 kg)
    · 04 Double Dumbbell Thruster (17.5 kg)
    · 03 DBall Squat (100 lb)
    · 03 DBall Clean (100 lb)
    1 min rest
/****************************************************************************/
Lunes 06-05-2019
- 3 Sets
    · 04 Zombie Squat T:2-2-2 (115 lb)
    · 12 (6+6) One Hand Kettlebell Row (20 kg)
    · 05 Step Burpees
- 5 Sets (3 min per set)
    · 10 Cal Assault
    · 10 Step Burpees Over Parallel
    · 14 Dumbbell Snatch (17.5 kg)
    · 12 mt DBall Carry (150 lb)
/****************************************************************************/
Martes 07-05-2019
- EMOM 8 min
    · 02 Squat Clean + 02 Hang Power Clean + 02 Short Hang Squat Clean (115 lb)
    · 12 (6+6) Kettlebell Snatch
- For Time
    02 - 04 - 06 - 08 - 10 - 12 - 14 - 16 - 18 - 20
    · Double Dumbbell Box Step Up (17.5 kg)
    · Lateral Box Jump
- 3 Sets
    · 16 Thruster (75 lb)
    · 16 American Swing (24 kg)
    · 16 Toes to Bar
    · 16 Burpees
    · 02 DBall Squat (150 lb)
/****************************************************************************/
Miércoles 08-05-2019
- 3 Sets
    · 04 DBall Squat T:3-3-3 (100 lb)
    · 06 (3+3) One Hand Dumbbell Thruster T:3-1-1 (20 kg)
    · 06 (3+3) One Leg Kettlebell Round Body (16 kg)
- EMOM 16 min
    · 50 s Row
    · 14 Dumbbell Thruster (15 kg)
    · 12 Burpees Over Parallel
    06 - 07 - 08 - 09
    · DBall Squat (100 lb)
- 3 Sets
    · 10 (5+5) One Leg Rise Wall Sit
/****************************************************************************/
Jueves 09-05-2019
- EMOM 8 min
    · 06 Clusthers (95 lb)
    · 08 DBall Clean (100 lb)
- For Time (in pairs)
    · 100 Hang Power Clean (105 lb)
- EMOM 20 min
    · 10 DeadLift Snatch Grip (115 lb)
    · 20 Knees to Elbow
    · 15 Burpees
    · 08 Strict Pull Up
/****************************************************************************/
Viernes 10-05-2019
- 7 Sets
    · 01 Power Snatch (115 lb)
- 7 Sets
    · 01 Power Snatch + 01 Hang Snatch + 01 Over Head Squat (105 lb)
- 3 Sets
    · 01 Hang Power Clean + 01 Power Clean
    > 1st: 115 lb        > 2nd: 135 lb
    > 3rd: 155 lb
- 5 Sets
    · 01 Power Clean
    > 1st: 145 lb        > 2nd: 155 lb
    > 3rd: 165 lb        > 4th: 175 lb
    > 5th: 195 lb
/****************************************************************************/
Sábado 11-05-2019
- EMOM 15 min
    · 10 Seated Arnold Press (12.5 kg)
    · 08 Lateral Fly (7.5 kg)
    · 08 Strict Pull Up
- 4 Sets
    · 15 Reverse Crunch
- 4 Sets
    · 15 s Lateral Plank Right Hand
    · 15 s Lateral Plank Left Hand
- For Time
    in groups of 4
    · 200 Cal Assault
    · 300 GHD Sit Ups
    · 100 DBall Squat (100 lb)
- 10 Sets
    · 40 s Work
    · 20 s Rest
/****************************************************************************/
Lunes 13-05-2019
- 3 Sets
    · 04 Back Squat T:3-1-3 (205 lb)
    · 06 Seated Arnold Press T:3-1-3 (12.5 kg)
    · 08 (4+4) Banded Semi Flex One Leg T:3-3-3
- 5 Sets
    · 10 Cal Assault
    · 30 Air Squat
    · 10 Devil Press (12.5 kg)
    · 16 Wall Ball (9 kg)
    > 1 min rest
/****************************************************************************/
Martes 14-05-2019
- EMOM 8 min
    · 08 Fat Bar Clean & Jerk (90 lb)
    · 10 Double Kettlebell American Swing (16 kg)
- For Time
    21 - 15 - 09
    · DBall Clean (100 lb)
    · Dumbbell Push Press (15 kg)
    · Toes to Bar
- 3 Sets
    · 20 Fat Bar Front Rack Reverse Lunge (50 lb)
    · 20 Dumbbell Snatch (20 kg)
    · 10 Double Dumbbell Burpees (15 kg)
    · 02 Short Hang Squat Clean (115 lb)
    > 1 min rest
/****************************************************************************/
Miércoles 15-05-2019
- 5 Sets
    · 01 Paused Power Snatch + 01 Hang Power Snatch (85 lb)
    > 3 s Knees
    > 3 s Over Knees
- 5 Sets
    · 01 Paused Hang Squat Clean T:4-0 (165 lb)
- 5 Sets
    · 01 Split Jerk (105 lb)
/****************************************************************************/
Jueves 16-05-2019
- EMOM 8 min
    · 10 DeadLift (225 lb)
    · 10 Hang Power Snatch (75 lb)
- For Time
    02 - 04 - 06 - 08 - 10 - 12 - 14 - 16
    · Dumbbell Snatch (20 kg)
    · Toes to Bar
- 3 Sets
    · 16 Thrusters (75 lb)
    · 16 Burpees Over Bar
    · 16 Push Ups
    · 03 DBall Clean (150 lb)
    > 1 min rest
/****************************************************************************/
Sábado 18-05-2019
- So much technique
- 4 Sets
    · 03 Muscle Snatch (65 lb)
- 5 Sets
    · 02 Power Clean (145 lb)
    · 05 DBall Squat (70 lb)
- 3 Sets
    · 06 (3+3) Dumbbell Push Press T:2-2-1 (17.5 kg)
/****************************************************************************/
Lunes 20-05-2019
- 3 Sets
    · 04 Thrusters T:3-1-1 (95 lb)
    · 06 Glute Bridge + Dumbbell Press T:2-2-4 (15 kg)
    · 04 Burpees + Dumbbell Swing (15 kg)
- 4 Sets (4 min per set)
    · 50 s Row
    · 16 Thruster (75 lb)
    · 30 Air Squat
    · 10 Devil Press (15 kg)
- 3 Sets
    · 04 Slow Swimmers T:3-3
/****************************************************************************/
Miércoles 22-05-2019
- 3 Sets
    · 04 Sumo DeadLift T:1-3-3 (215 lb)
    · 12 (6+6) One Hand Ring Row
    · 02 (1+1) Dumbbell Snatch + 10 s Hold (30 kg)
- 4 Sets
    · 12 Fat Bar DeadLift (140 lb)
    · 10 Burpees Over Bar
    · 15 Knees to Elbow
    · 14 Dumbbell Snatch (17.5 kg)
    · 03 DBall Clean (120 lb)
- For Time
    · 100 Air Squat
/****************************************************************************/
Jueves 23-05-2019
- EMOM 8 min
    · 10 Split Jerk Balance (75 lb)
    · 10 Dumbbell Front Squat (20 kg)
- EMOM 8 min
    · 08 Squat Clean (115 lb)
    · 12 (6+6) Split Push Press (20 kg)
- For Time (in trios)
    · 240 Cal Assault
    · 150 Push Ups
    · 075 Hang Power Clean (145 lb)
/****************************************************************************/
Viernes 24-05-2019
- 3 Sets
    · 02 Power Clean + 02 Hang Power Clean (165 lb)
    · 04 Dumbbell Push Press T:1-2-3 (20 kg)
    · 05 Wall Ball T:5-0-1 (9 kg)
- 4 Sets
    01 + 02 + 03 + 04
    · DeadLift + Hang Power Clean (135 lb)
    · 14 Dumbbell Push Press (15 kg)
    · 14 Dumbbell Front Squat (15 kg)
    · 14 Wall Ball (9 kg)
    > 1 min rest
- 3 Sets
    · 15 s Lateral Plank RH
    · 15 s Lateral Plank LH
/****************************************************************************/
Sábado 25-05-2019
- EMOM 20 min
    · 08 Bench Dumbbell Press (12.5 kg)
    · 08 Barbell Row (75 lb)
    · 08 Banded Ring Row
    · 30 s Push Ups
- 10 Sets
    · 15 s Fast Feets Slow Hands
    · 15 s Skipping
    · 90 s Lateral Skipping Bumper Step
- 7 Sets
    90 s Work + 30 s Rest
    · 07 Cal Assault
    · 07 Kettlebell Russian Swing (32 kg)
    · 07 Jumping Squat
    · 07 Dumbbell Push Press (12.5 kg)
    · AMRAP Burpees
/****************************************************************************/
Lunes 27-05-2019
- 3 Sets
    · 03 Zombie Squat T:3-2-3 (115 lb)
    · 06 (3+3) One Hand Devil Press (17.5 kg)
    · 12 mt DBall Carry (120 lb)
- 5 Sets
    · 10 Cal Assault
    · 30 Air Squat
    · 10 Devil Press (15 kg)
    · 10 DBall Squat (80 lb)
    > 1st: 45 s rest   > 2nd: 50 s rest
    > 3rd: 55 s rest   > 4th: 60 s rest
/****************************************************************************/
Martes 28-05-2019
- 3 Sets
    · 02 Power Clean t&g (185 lb)
- Fibonacci 12 min
    00 - 02 min
    · 12 Burpee Over Bar
    02 - 05 min
    · 12 Burpee Over Bar + 08 Power Clean (95 lb)
    05 - 08 min
    · 12 Burpee Over Bar + 08 Power Clean (95 lb) + 20 Dumbbell Snatch (20 kg)
    08 - 12 min
    · 12 Burpee Over Bar + 08 Power Clean (95 lb) + 20 Dumbbell Snatch (20 kg) + 20 Toes to Bar
> 2 min rest
- EMOM 12 min
    · 12 Cal Assault
    > 1 min rest
/****************************************************************************/
Jueves 30-05-2019
- EMOM 18 min
    · 08 Fat Bar Clean & Jerk (50 lb)
    · 16 American Swing (24 kg)
    · 50 s (25+25) Dumbbell Hold (35 kg)
- To Finish
    · 150 Push Up
    · 05 Back Squat (145 lb) every time break
/****************************************************************************/
Lunes 03-06-2019
- 3 Sets
    · 04 Front Squat T:2-2-2 (175 lb)
    · 04 Devil Press + Thruster (15 kg)
    · 08 Kettlebell Push Hold
- 4 Sets (4 min per set)
    · 50 s Row
    · 16 Double Kettlebell Russian Swing (16 kg)
    · 08 Devil Press + Thruster (15 kg)
    · 10 Cal Assault
- 2 Sets
    · 05 Slow Swimmers T:3-3
/****************************************************************************/
Martes 04-06-2019
- EMOM 12 min
    · 12 DBall Squat (80 lb)
    · 15 Toes to Bar
    · 12 12 DBall Clean (100 lb)
    · 20 GHD Sit Ups
- EMOM 12 min
    · 16 Thrusters (75 lb)
    · 10 Strict Pull Up
    · 10 Glute Bridge Shoulder Press (17.5 kg)
- 3 Sets
    Hollow Rock
    · 20 s Bended Legs + Hands to Knees
    · 20 s Extended Legs + Hands to Knees
    · 20 s Bended Legs + Hands to Back
    · 20 s Extended Legs + Hand to Back
    · 20 s Extended Legs + Hand to Back + Swing
/****************************************************************************/
Jueves 06-06-2019
- 4 Sets
    · 12 Single Arm Walking Lunge Dumbbell Over Head (22.5 kg)
    · 12 mt DBall Carry (150 lb)
    · 10 Single Arm Dumbbell Burpees + Dumbbell Snatch (22.5 kg)
- For Time (in pairs)
    · 50 Squat Clean (145 lb)
- 4 Sets
    · 20 Row Pull
    · 12 Step Burpees Over Parallel
    · 12 (6+6) Single Arm Dumbbell Front Squat (22.5 kg)
    · 01 Squat Clean + 01 Hang Squat Clean + 01 Short Hang Squat Clean (145 lb)
/****************************************************************************/
Sábado 08-06-2016
- 3 Sets
    · 12 (6+6) Kettlebell Up Hold Shoulder Press (8 kg)
    · 12 mt Kettlebell Over Head Up Hold Carry (8 kg)
    · 01 Power Clean + 01 Hang Power Clean + 01 Short Power Clean (115 lb)
- 4 Sets
    90 s Work - 30 s Rest
    · 30 s Right Hand Dumbbell Hang Snatch (17.5 kg)
    · 30 s Left Hand Dumbbell Hang Snatch (17.5 kg)
    · 10 s Skipping
    · 10 s Fast Feets Slow Hands
    · 10 s Resorte
- 4 Sets
    · 05 DBall Clean (120 lb)
    · 07 Dumbbell Thrusters (15 kg)
    · 05 DBall Squat (120 lb)
- 4 Sets
    20 s Work - 10 s Rest
    · Assault
    · Burpees
    · Jumping Squat
    · Jumping Lunge
    · Push Ups
/****************************************************************************/
Lunes 10-06-2019
- 3 Sets
    · 04 Front Squat T:3-2-2 (165 lb)
    · 10 Dumbbell Glute Bridge (30 kg)
    · 04 Complex
        · 01 Push Up + 01 Burpee
- 4 Sets (4 min per set)
    · 14 Double Kettlebell Russian Swing (16 kg)
    · 14 Push Ups
    · 08 Double Kettlebell Front Squat (16 kg)
    · 08 Burpees to Plate
    · 15 Wall Ball (9 kg)
/****************************************************************************/
Martes 11-06-2019
- EMOM 10 min
    · 06 DeadLift (205 lb)
    · 06 Dumbbell Hang Power Clean Step Over Bar (22.5 kg)
    · 03 Wall Climb
- For Time
    · 100 Air Squat
    · 80 Lunge
    · 60 Parallel Jump
    · 40 Sit Up
    · 20 Dumbbell Snatch (22.5 kg)
    · 10 DeadLift Snatch Grip (205 lb)
- 3 Sets
    · 16 (8+8) Single Arm Dumbbell Power Clean + Push Press (20 kg)
    · 16 American Swing (24 kg)
    · 16 (8+8) Single Arm Dumbbell Hang Squat Clean + Push Press (20 kg)
    · 10 Toes to Bar
/****************************************************************************/
Miércoles 12-06-2019
- 4 Sets
    · 01 Power Clean + 02 Hang Squat Clean (135 lb)
    · 05 Arnold Press (12.5 kg)
- 4 Sets
    50 s - 40 s - 30 s - 20 s
    · Assault
    · Dumbbell Snatch (15 kg)
    · Thrusters (65 lb)
    · Parallel Jump
- 3 Sets
    · 14 (7+7) Glute Bridge Leg Rise
/****************************************************************************/
Jueves 13-06-2019
- 4 Sets
    Left Arm
    · 06 Dumbbell Snatch (22.5 kg)
    · 05 Dumbbell Hang Power Clean (22.5 kg)
    · 04 Dumbbell Push Press (22.5 kg)
    · 06 mt Dumbbell Over Head Carry (22.5 kg)
    Right Arm
    · 06 Dumbbell Snatch (22.5 kg)
    · 05 Dumbbell Hang Power Clean (22.5 kg)
    · 04 Dumbbell Push Press (22.5 kg)
    · 06 mt Dumbbell Over Head Carry (22.5 kg)
- 4 Sets
    · 08 DeadLift (115 lb)
    · 03 DBall Clean (150 lb)
    · 06 Power Clean (115 lb)
    · 02 DBall Clean (150 lb)
    · 04 Squat Clean (115 lb)
    · 01 DBall Clean (150 lb)
- AMRAP 1 min
    · Cal Assault (30 cal)
- AMRAP 1 min
    · Burpees (22)
/****************************************************************************/
Sábado 15-06-2019
- 3 Sets
    08 - 12
    · Sumo Rumanian Double Kettlebell Row (16 kg)
    · Seated Shoulder Press (12.5 kg)
    · Glute Bridge BarBel Press (95 lb) 
- Tabata x2
    20 s Work + 10 s Rest
    · Dumbbell Thrusters (12.5 kg)
    · Dball Clean & Jerk (60 lb)
- 6 Sets
    90 s Work + 30 s Rest
    · 15 s Fast Feets Slow Hands ~85%
    · 15 s Fast Feets Slow Hands ~100%
    · 30 s Sit Ups
    · 30 Frontal Plank / Hollow Rock / Lateral Plank x2
- 5 Sets
    · 05 Goblet Dumbbell Squat T:5-0-5 (30 kg)
    · 20 s Jumping Squat / Jumping Lunges / Isometric Lunge
    · 30 s Isometric Wall Squat
    · 05 DBall Squat T:5-0-5 (30 kg)
    · 20 s Jumping Squat / Jumping Lunges / Isometric Lunge
    · 30 s Isometric Wall Squat
/****************************************************************************/
Martes 18-06-2019
- AMRAP 4 min
    · 06 Lying with Bumper Over Head - Legs to Bumper (25 lb)
    · 06 Legs Rise Bumper Rotation (25 lb)
    · 06 Sit Up with Bumper (25 lb)
- For Time Ladder
    · 10 s Power Snatch (75 lb)
    · 15 s Power Snatch (75 lb)
    · 20 s Power Snatch (75 lb)
    · 15 s Power Snatch (75 lb)
    · 10 s Power Snatch (75 lb)
- For Time Ladder
    · 10 s Front Squat (75 lb)
    · 15 s Front Squat (75 lb)
    · 20 s Front Squat (75 lb)
    · 15 s Front Squat (75 lb)
    · 10 s Front Squat (75 lb)
- For Time (TC: 6 min)
    21 - 15 - 09
    · Thrusters (75 lb)
    · Burpees Over Bar
- EMOM 6 min
    · 50 s Row
    · 15 Air Squat + 10 Sit Up + 05 Push Up
/****************************************************************************/
Jueves 20-06-2019
- 4 Sets
    · 01 Squat Clean (185 lb)
    · 06 (3+3) Shoulder Press Tip TOp (17.5 kg)
- In Pairs
    For Time
        · 60 Power Clean (135 lb)
    4 Sets
        · 14 Dumbbell Clusters (12.5 kg)
        · DeadLift Hold while other work (135 lb)
    For Time
        · 60 Toes to Bar
        · Hang Hold while other work
/****************************************************************************/
Viernes 21-06-2019
- 4 Sets
    Right Hand
    · 07 Dumbbell Snatch (20 kg)
    · 06 Dumbbell Hang Power Clean (20 kg)
    · 05 Dumbbell Push Press (20 kg)
    · 04 Dumbbell Over Head Static Lunge T:3-1-3
    Left Hand
    · 07 Dumbbell Snatch (20 kg)
    · 06 Dumbbell Hang Power Clean (20 kg)
    · 05 Dumbbell Push Press (20 kg)
    · 04 Dumbbell Over Head Static Lunge T:3-1-3
- 5 Sets (3 min per set)
    · 12 Cal Assault
    · 08 Step Burpees Over Parallel
    · 12 Dumbbell Front Lunge (15 kg)
/****************************************************************************/
Sábado 22-06-2019
- 4 Sets
    · 01 Power Snatch (95 lb)
    · 02 Hang Power Snatch (95 lb)
- EMOM 12 min
    00 to 06 min
    · 10 Hang Power Snatch (75 lb)
    · 15 American Swing + 10 Kettlebell DeadLift + 05 Kettlebell Goblet Squat (20 kg)
    06 to 12 min
    · 10 Hang Power Clean (75 lb)
    · 15 American Swing + 10 Kettlebell DeadLift + 05 Kettlebell Goblet Squat (20 kg)
- 3 Sets
    · 20 s Legs Over Dumbbell
    · 20 s Legs Crunch Dumbbell
/****************************************************************************/
Lunes 24-06-2019
- 3 Sets
    · 05 Front Squat T:3-2-2 (145 lb)
    · 08 (4+4) Dumbbell Push Press T:1-1-3 (20 kg)
- 5 Sets
    AMRAP 2 min
    · 14 Cal Assault
    · 14 Knees to Chest
    · to 2 min Thrusters (75 lb)
    > 1 min rest
/****************************************************************************/
Martes 25-06-2019
- 3 Sets
    · 01 Power Clean + 01 Hang Power Clean + 02 Thruster (115 lb)
    · 06 Double Kettlebell Russian Swing + 05 Kettlebell Front Squat (20 kg)
- For Time (TC: 6 min)
    AMRAP 5 min
    · Cal Assault
    > 1 min rest
- To Finish (TC: 6 min)
    · 30 Wall Ball (9 kg)
    · 10 American Swing (20 kg)
    · 20 Wall Ball (9 kg)
    · 20 American Swing (20 kg)
    · 10 Wall Ball (9 kg)
    · 30 American Swing (20 kg)
- To Finish (TC: 6 min)
    · 30 Sit Up
    · 10 Front Squat (115 lb)
    · 15 Burpees Over Bar
    · 30 Sit Up
    · 10 Thruster (115 lb)
- 3 Sets
    · 40 s Hollow Rock + 01 Crunch every 5 sec
/****************************************************************************/
Jueves 27-06-2019
- 3 Sets
    · 03 Power Clean & Jerk (115 lb)
    · 06 (3+3) Dumbbell Snatch (30 kg)
- For Time (TC: 7 min)
    · 30 Row Pull
    · 10 Dumbbell Snatch (20 kg)
    · 20 Row Pull
    · 20 Dumbbell Snatch (20 kg)
    · 10 Row Pull
    · 30 Dumbbell Snatch (20 kg)
    > every minute 02 Burpees over Row
- For Time (TC: 7 min)
    · 30 Toes to Bar / L hang Hold
    · 09 Power Clean & Jerk (95 lb)
    · 20 Toes to Bar
    · 12 Power Clean & Jerk (95 lb)
    · 10 Toes to Bar
    · 15 Power Clean & Jerk (95 lb)
    > every minuto 02 Burpees over Bar
- 3 Sets
    · 10 (5+5) Semi Flexor Sit Leg Rise
/****************************************************************************/
Viernes 28-06-2019
- 4 Sets
    · 10 (5+5) One Hand Band Row + Other Hand Arnold Press (10 kg)
    · 06 (3+3) Bench Press Tip Top (20 kg)
- EMOM 16 min
    · 10 Bench Press T:2-1-2 (15 kg)
    · 10 Burpees
    · 10 Pull Ups
    · 30 s Assault ~80%
- 3 Sets
    · 10 Semi Flexion Glute Bridge + 10 s Hold
    · 10 (5+5) Glute Bridge One Leg Rise
/****************************************************************************/
