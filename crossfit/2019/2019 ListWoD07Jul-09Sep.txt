/****************************************************************************/
Lunes 01-07-2019
- 4 Sets
    · 06 Rumanian DeadLift T:4-1-1 (135 lb)
    · 20 Dumbbell Glute Bridge (30 kg)
- For Time (TC: 12 min)
    · 10 American Swing (24 kg)
    · 25 Kettlebell Sumo DeadLift High Pull (24 kg)
    · 20 Sit Up
    · 15 American Swing (24 kg)
    · 20 Kettlebell Sumo DeadLift High Pull (24 kg)
    · 20 Sit Up
    · 20 American Swing (24 kg)
    · 15 Kettlebell Sumo DeadLift High Pull (24 kg)
    · 20 Sit Up
    · 25 American Swing (24 kg)
    · 10 Kettlebell Sumo DeadLift High Pull (24 kg)
    > 02 Burpees every minute
    > Burpees until the last end wod
/****************************************************************************/
Martes 02-07-2019
- 5 Sets
    · 12 Dumbbell Snatch (25 kg)
    · 20 Parallel Jump
    · 14 Knees to Elbow
- 4 Sets
    · 03 + 1 Power Clean & Jerk (115 lb)
    · 16 Push Ups
    · 03 + 1 Power Clean & Jerk (115 lb)
    · 16 Bumper Over Head Lunge
    · 14 Wall Ball (10 kg)
- AMRAP 90 s
    · Wall Ball (9 kg)
/****************************************************************************/
Jueves 04-07-2019
Performance
- 5 Sets
    · 45 s Back Squat (135 lb)
    · 45 s Seated Arnold Press (12.5 kg)
    · 30 s Plank
- Emom 8 min
    · 10 Dumbbell Hang to Over Head + 03 Burpees (15 kg)
    · 07 DBall Clean + 03 Burpees (100 lb)
- 4 Sets
    · 10 Cal Assault
    · 10 Dumbbell Thruster (15 kg)
------------------------------------------------------------------------------
Olympic
- 3 Sets
    · 02 Front Squat + 02 Push Press + 02 Thruster (95 lb)
    · 08 (4+4) Lunge Dumbbell Snatch (15 kg)
    · 10 Row Pull Banded
- 4 Sets
    · 50 s Row
    · 16 Thruster (75 lb)
    · 16 Dumbbell Snatch (15 kg)
    · 05 + 1 DBall Squat (80 lb)
/****************************************************************************/
Martes 09-07
- 4 Sets
    · 12 Cal Assault
    · 12 DeadLift (95 lb)
    · 08 Hang Power Clean (95 lb)
    · 06 Power Jerk (95 lb)
- 4 Sets
    · 15 American Swing (24 kg)
    · 10 Kettlebell DeadLift (24 kg)
    · 05 Kettlebell Goblet Squat (24 kg)
    · 40 Jump to Plate
    · 10 Toes to Bar
    · 05 Burpees to Bar
/****************************************************************************/
Jueves 11-07-2019
- EMOM 8 min (1 - 1)
    > 1 min work
        · 15 s Assault ~90%
        · 06 Complex
            · 01 Devil Press + Dumbbell Thruster (12.5 kg)
        · 01 Squat Clean + 01 Hang Squat Clean (115 lb)
    > 1 min rest
- EMOM 6 min
    · 20 Jump to Plate + 10 Plate Thruster + 10 Plate Sit Up
- 4 Sets
    · 05 DBall Clean + 03 DBall Squat (80 lb)
/****************************************************************************/
Sábado 13-07-2019
- 7 Sets
    · 04 Dumbbell Clean (15 kg)
    · 04 Dumbbell Floor to Over Head (15 kg)
    · 05 Dumbbell Thruster (15 kg)
    · 10 Wall Ball (9 kg)
- For Time (TC: 8 min)
    · 30 Knees to Elbow
    · 10 Hang Power Clean (95 lb)
    · 20 Knees to Elbow
    · 15 Hang Power Clean (95 lb)
    · 10 Knees to Elbow
    · 20 Hang Power Clean (95 lb)
    > 02 Burpees every minute
- For Time (TC: 8 min)
    · 40 Row Pull
    · 10 American Swing (24 kg)
    · 35 Row Pull
    · 20 American Swing (24 kg)
    · 30 Row Pull
    · 30 American Swing (24 kg)
    > 02 Burpees every minute
/****************************************************************************/
Lunes 15-07-2019
- 4 Sets
    · 05 Back Squat T:3-1-1 (175 lb)
    · 06 (3+3) Dumbbell Thruster Single Arm T:3-1-1 (30 kg)
    · 12 s Skipping
- AMRAP 12 min
    first 6 min
    04 - 08 - 12
    · Dumbbell Thruster (15 kg)
    · Burpees
    · Sit Ups
    · DBall Clean (100 lb)
    last 6 min
    12 - 08 - 06
    · Dumbbell Thruster (15 kg)
    · Burpees
    · Sit Ups
    · DBall Clean (100 lb)
/****************************************************************************/
Martes 16-07-2019
- 7 Sets
    · 04 Dumbbell Power Clean (15 kg)
    · 04 Dumbbell to Over Head (15 kg)
    · 05 Dumbbell Thruster (15 kg)
    · 10 Wall Ball (9 kg)
- To Finish (TC: 8 min)
    · 30 Toes to Bar
    · 10 Hang Power Clean (95 lb)
    · 20 Toes to Bar
    · 15 Hang Power Clean (95 lb)
    · 10 Toes to Bar
    · 20 Hang Power Clean (95 lb)
    > 02 Burpees every minute
- To Finish (TC: 8 min)
    · 40 Row Pull
    · 10 American Swing (24 kg)
    · 35 Row Pull
    · 20 American Swing (24 kg)
    · 30 Row Pull
    · 30 American Swing (24 kg)
    > 02 Burpees every minute
/****************************************************************************/
Jueves 18-07-2019
- 6 Sets
    · 10 DeadLift (135 lb)
    · 06 Burpees Over Bar
    · 16 Dumbbell Snatch (20 kg)
- 5 Sets (2 min per set)
    · 08 Dumbbell Power Clean (15 kg)
    · 06 Dumbbell Lunge (15 kg)
    · 14 Knees to Elbow
    · 08 Dumbbell Power Clean (15 kg)
    · 08 Dumbbell Front Squat (15 kg)
- 4 Sets
    · 05 Slow Swimmers T:4-4
/****************************************************************************/
Martes 23-07-2019
- 4 Sets
    · 06 Floor Press T:2-2-2 (95 lb)
    · 03 DBall Clean & Jerk + Floor Slam (70 lb)
    · 06 (3+3) Dumbbell Split Jerk (25 kg)
- 5 Sets
    · 14 Push Up
    · 03 DBall Clean (100 lb)
    · 03 Complex (r)
        · 01 Dumbbell Burpee + 01 Dumbbell Snatch + 01 Dumbbell Push Pres + 01 Dumbbell Split Jerk (15 kg)
    · 02 DBall Clean (100 lb)
    · 03 Complex (l)
        · 01 Dumbbell Burpee + 01 Dumbbell Snatch + 01 Dumbbell Push Pres + 01 Dumbbell Split Jerk (15 kg)
    · 01 DBall Clean (100 lb)
    · 30 Bumper Box
/****************************************************************************/
Miércoles 24-07-2019
- 4 Sets
    · 02 Power Snatch + 01 Hang Power Snatch (75 lb) 2 s Hold
    · 06 (3+3) Single Arm Dumbbell Static Lunge T:3-1-3 (20 kg)
    · 10 s Fast Feets Slow Hands
- 5 Sets (3 min per set)
    · 50 s Row
    · 12 Front Dumbbell Lunge (15 kg)
    · 15 Wall Ball (9 kg)
    · 06 + 1 Hang Power Snatch (75 lb)
/****************************************************************************/
Jueves 25-07-2019
- EMOM 12 min
    · 01 Squat Clean + 01 Hang Squat Clean + 03 Front Squat + 06 Burpees Over Bar (125 lb)
    · 15 American Swing + 10 Kettlebell DeadLift + 05 Goblet Squat (24 kg)
- For Time 12 min
    a) EMOM 6 min
    · 03 Wall Climb
    · 16 (8+8) Dumbbell Hang Power Clean & Jerk (17.5 kg)
    b) EMOM 6 min
    · 30 s Hand Stand Hold
    · 16 (8+8) Dumbbell Thrusters (17.5 kg)
- To Finish
    · 10 s Hollow Rock
    · 10 s Hollow Rock Knife
    · 10 s Hollow Rock Scissor
    · 15 s Hollow Rock
    · 15 s Hollow Rock Knife
    · 15 s Hollow Rock Scissor
    · 10 s Hollow Rock
    · 10 s Hollow Rock Knife
    · 10 s Hollow Rock Scissor
/****************************************************************************/
Lunes 29-07-2019
- 4 Sets
    · 06 (3+3) Dumbbell Push Press (22.5 kg)
    · 05 DBall Clean & Jerk + Floor Slam (70 lb)
    · 10 (5+5) Kettlebell Drag Plank
- 6 Sets
    · 10 s Fast Feets Slow Hands
    · 04 Right Dumbbell Clean & Jerk (22.5 kg)
    · 06 Toes to Bar
    · 10 Wall Ball (9 kg)
    · 06 Toes to Bar
    · 04 Left Dumbbell Clean & Jerk (22.5 kg)
    · 15 Bumper Jump
/****************************************************************************/
Martes 30-07-2019
- For Time (TC: 7 min)
    21 - 15 - 09
    · American Swing (24 kg)
    · Goblet Squat (24 kg)
    > 03 Burpees every minute
- For Time (TC: 7 min)
    21 - 15 - 09
    · Thrusters (65 lb)
    · Dumbbell Snatch (17.5 kg)
    > 02 Burpees Over Bar every minute
- EMOM 10 min
    · 20 s Assault ~100% + 05 Dumbbell DeadLift + 06 Dumbbell Power Clean + 05 Dumbbell Front Squat (15 kg)
    · Rest
- AMRAP 4 min
    · 05 Lying with Bumper Over Head - Legs to Bumper (15 lb)
    · 06 Legs Rise Bumper Rotation (15 lb)
    · 05 Sit Up with Bumper (15 lb)
/****************************************************************************/
Miércoles 31-07-2019
- 3 Sets
    · 02 DeadLift + 02 Hang Power Clean + 02 Front Squat T:5-2-3 (115 lb)
    · 07 Dumbbell Glute Bridge 5s Hold (35 kg)
    · 02 DBall Clean (150 lb)
- AMRAP 16 min
    > AMRAP 4 min
        · Row T:1-2
    > AMRAP 4 min
        02 - 04 - 06 ...(+2)
        · DeadLift (95 lb)
        · Hang Power Clean (95 lb)
        · Front Squat (95 lb)
        · DBall Clean (100 lb)
    > AMRAP 4 min
        · Assault > 300 Ws
    > AMRAP 4 min
        03 - 06 - 09 ...(+3)
        · DeadLift (95 lb)
        · Hang Power Clean (95 lb)
        · Front Squat (95 lb)
        · DBall Clean (100 lb)
/****************************************************************************/
Jueves 01-08-2019
- 5 Sets
    · 05 + 01 Devil Press (15 kg)
    · 20 Jump Over Bar
    · 01 Power Snatch + 04 Hang Power Snatch (95 lb)
- For Time (in Pairs)
    · 080 DeadLift (115 lb)
    · 100 Wall Ball (9 kg)
    · 060 Power Clean (115 lb)
- To Finish
    · 30 s Plank with Bumber (25 lb)
    · 40 s Plank with Bumber (25 lb)
    · 50 s Plank with Bumber (25 lb)
/****************************************************************************/
Sábado 03-08-2019
- 06 Sets
    · 01 Power Clean + 01 Hang Squat Clean + 02 Front Squat (115 lb)
    · 10 Burpees Over Bar
    · 01 Squat Clean (115 lb)
- 06 Sets
    · 06 Dumbbell Sumo DeadLift (12.5 kg)
    · 05 Dumbbell Power Clean (12.5 kg)
    · 04 Dumbbell Floor to Over Head (12.5 kg)
    · 06 Dumbbell Walking Lunge (12.5 kg)
    · 10 Toes to Bar
    · 06 mt Dumbbell Carry
- EMOM 12 min
    · 20 American Swing (20 kg)
    · 15 Air Squat + 10 Sit Up + 05 Push Up
- To Finish
    10 s - 15 s - 10 s
    · Legs Over Dumbbell
    · Legs Crunch Dumbbell
    · Hollow Rock
/****************************************************************************/
Lunes 05-08-2019
- 3 Sets
    · 05 Front Squat T:3-1-2 (165 lb)
    · 06 Dumbbell Push Press T:1-2-5 (12.5 kg)
    · 06 Push Press T:1-2-5
- EMOM 16 min
    · 10 Wall Ball (9 kg) + 08 Push Ups
    · 10 Dumbbell Goblet Reverse Lunge (35 kg) + 10 Jumping Lunge
    · 20 Russian Swing + 12 mt Single Arm Kettlebell Carry (32 kg)
    · 05 + 1 Devil Press + 06 Dumbbell Front Squat (12.5 kg)
- 1 Set
    · 07 Slow Swimmers T:5-5
/****************************************************************************/
Martes 06-08-2019
- To Finish (in Pairs)
    · 100 Hang Power Snatch (75 lb)
    · Double Kettlebell Hold while other work (24 kg)
- 10 min Work
    00 to 02
    · 15 Dumbbell Thrusters (15 kg)
    02 to 04
    · 15 Dumbbell Thrusters (15 kg)
    · 10 Dumbbell Power Clean (15 kg)
    04 to 07
    · 15 Dumbbell Thrusters (15 kg)
    · 10 Dumbbell Power Clean (15 kg)
    · 15 Dumbbell Front Squat (15 kg)
    07 to 10
    · 15 Dumbbell Thrusters (15 kg)
    · 10 Dumbbell Power Clean (15 kg)
    · 15 Dumbbell Front Squat (15 kg)
    · 10 Dumbbell Push Press (15 kg)
- 10 min Work
    00 to 02
    · 14 Burpees Over Bar
    02 to 04
    · 14 Burpees Over Bar
    · 08 Hang Power Clean (95 lb)
    04 to 07
    · 14 Burpees Over Bar
    · 08 Hang Power Clean (95 lb)
    · 16 Dumbbell Snatch (20 kg)
    07 to 10
    · 14 Burpees Over Bar
    · 08 Hang Power Clean (95 lb)
    · 16 Dumbbell Snatch (20 kg)
    · 15 Knees to Elbow
- 2 Sets
    · 20 s Plank
    · 15 s Left Side Plank
    · 15 s Right Side Plank
/****************************************************************************/
Miércoles 07-08-2019
- 3 Sets
    · 05 DeadLift T:3-1-2 (185 lb)
    · 10 Double Kettlebell Front Rack Lunge (16 kg)
    · 15 s Skipping ~100%
- 5 Sets (3 min per set)
    · 12 + 02 Kettlebell DeadLift High Pull (24 kg)
    · 12 + 02 American Swing (24 kg)
    · 20 Parallel Jump
    · 12 + 02 Single Arm Kettlebell Walking Lunge (24 kg)
- 1 Set
    · 40 s Plank
/****************************************************************************/
Jueves 08-08-2019
- 4 Sets
    · 10 Dumbbell Thruster (15 kg)
    · 03 DBall Clean (150 lb)
    · 10 Wall Ball (9 kg)
- To Finish
    · 30 Double Dumbbell Snatch (15 kg)
    · 30 Toes to Bar
    · 30 Power Clean & Jerk (75 lb)
    · 30 Toes to Bar
- AMRAP 8 min
    02 - 04 - 06 - 08 .. (+02)
    · DeadLift (165 lb)
    · Double Dumbbell Clusther (15 kg)
    · Burpees over Bar
/****************************************************************************/
Sábado 10-08-2019
- 4 Sets
    · 07 Right Dumbbell Clean (20 kg)
    · 06 Right Dumbbell Snatch (20 kg)
    · 05 Right Dumbbell Push Press (20 kg)
    · 06 Right Front Rack Walking Lunge (20 kg)
    · 07 Left Dumbbell Clean (20 kg)
    · 06 Left Dumbbell Snatch (20 kg)
    · 05 Left Dumbbell Push Press (20 kg)
    · 06 Left Front Rack Walking Lunge (20 kg)
- To Finish (TC: 12 min)
    14 - 12 - 10 - 08 - 06 - 04 - 02
    · Burpees over Bar WithOut Flexion
    · Thruster (75 lb)
- To Finish (TC: 12 min)
    02 - 04 - 06 - 08 - 10 - 12 - 14
    · Push Press (75 lb)
    · DBall Squat (80 lb)
- 3 Sets
    · 30 s Hollow Rock pasing Bumper over and down Leg (2.5 kg)
/****************************************************************************/
Sábado 28-09-2019
- For Time
    12 - 10 - 08 - 06 - 04 - 02
    · Power Clean & Jerk (65 lb)
    · Toes to Bar
    02 - 04 - 06 - 08 - 10 - 12
    · Hang Squat Clean (65 lb)
    · Burpees Over Bar
- AMRAP 06 min
    02 - 04 - 06 .. (+02)
    · Kettlebell Sumo DeadLift High Pull (16 kg)
    · American Swing (16 kg)
- 3 Sets
    Hollow Rock
    · 15 s Bended Legs + Hands to Heel
    · 15 s Bended Legs + Hands to Knees 
    > 30 s rest
/****************************************************************************/
